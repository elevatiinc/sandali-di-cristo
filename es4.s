#   --Authors--
# @Crippa Edoardo
# @Delle Foglie Alessandro
# @Lazzaroni Paolo
# @Zanni Simone
#

#Il programma si occupera'�di ricevere in input dati e, ogni 8 valori, ne
#calcolera' la media dando in output su un display (a 7 segmenti e 3 cifre)
#il valore.


#Segmento di dati in cui salvare i dati in input e output
#Per facilitare l'inserimento ogni variabile ha un suo preciso segmento di memoria

				.data 0x10010000			#inizio del segmento dati (0x10010000)
A_D:									#label della cella contenente A_D
				.data 0x10010004			#dati in 0x10010004
DATA_IN:								#label della cella contenente DATA_IN
				.data 0x10010008			#dati in 0x10010008
OUT_1:									#label di OUT dove mettera' i 3 dati in uscita

#Segmento di testo
				.text					#inizio del segmento di testo
main:

#Inizializzazione variabili sum e count e caricamento indirizzi label A_D e DATA_IN

				add $s0, $zero, $zero			#sum = 0
				add $s1, $zero, $zero			#count = 0
				la $t1, A_D				#carica in $t1 l'indirizzo di A_D
				la $t2, DATA_IN				#carica in $t2 l'indirizzo di DATA_IN

#Primo ciclo, che si ripete fino a quando il 12 bit della cella A_D e' diverso da 0
#Il controllo e' effettuato grazie a una maschera AND in cui solo il 12� bit e' settato a 1, gli altri a 0
#La maschera lascera' invariato il bit interessato, e mettera' a 0 tutti gli altri
#In questo modo il risultato dell'operazione sara' 0x0000 solo nel caso in cui il 12� bit sia 0

Check:				lh $t3, 0($t1)				#carica la half A_D in $t3
				andi $t3, $t3, 0x1000			#lascia il valore del 12esimo bit e gli altri a zero
				beq $t3, $zero, Check			#se zero torna a controllare

#Arrivati a questo punto significa che in A_D ha un valore accettabile, quindi possiamo leggere il dato da DATA_IN
#Ripetiamo per 8 volte fino a avere 8 dati e ogni volta aggiungiamo il numero trovato alla variable sum ($s0)
#Il ciclo viene fatto ripartire dal controllo del 12� bit di A_D

				lbu $t3, 0($t2)				#carica DATA_IN in $t3
				add $s0, $s0, $t3			#somma il dato attuale al valore della somma e lo mette in $s0
				addi $s1, $s1, 1			#incremento count ($s1)
				slti $t0, $s1, 8			#controlla se $s1 a' minore di 8 e se true setta a 1 il registro $t0
				bne $t0, $zero, Check			#salta a Check se $t0 a' diverso da 0 (cioe' in caso in cui ci sia un segnale di dato)
				
#A questo punto del programma abbiamo in $s0 la somma degli otto numeri, procediamo con la media

				li $t0, 8				#mettiamo in t0 il valore 8 per la divisione
				div $s2, $s0, $t0			#calcoliamo la media e la mettiamo in s2
				
#Dividiamo le cifre del numero ottenuto in unita'�, decine e centinaia
#Il numero dato dalla media viene diviso per 10 3 volte, e ogni volta viene tenuto il resto della divisione
#Questi saranno il valore delle unita', decine e centinaia

				li $t0, 10				#carichiamo il valore 10 in $t0
				div $s2, $t0				#dividiamo la media per 10 (per ottenere il quoziente e il resto (che sara'�l'unita'))
				mflo $t1				#carichiamo il quoziente in $t1
				mfhi $s3				#prendiamo il valore dell'unita'�e lo mettiamo in $s3
				div $t1, $t0				#dividiamo la media per 10 (per ottenere il quoziente e il resto (che sara'�la cifra delle decine))
				mflo $t1				#carichiamo il quoziente in $t1
				mfhi $s4				#prendiamo il valore delle decine e lo mettiamo in $s4
				div $t1, $t0				#dividiamo la media per 10 (per ottenere il quoziente e il resto (che sara'�la cifra delle centinaia))
				mfhi $s5				#prendiamo il valore delle centinaia e lo mettiamo in $s5

#Convertiamo in ascii i valori sommando 48 al numero ottenuto (48 e' il valore associato a 0 nella tabella ASCII)

				addiu $s3, $s3, 48			#trova l'ascii dell'unita'�e lo mette in $s3
				addiu $s4, $s4, 48			#trova l'ascii delle decine e lo mette in $s4
				addiu $s5, $s5, 48			#trova l'ascii delle centinaia e lo mette in $s5

#Salva gli ascii nelle posizioni di memoria

				la $t0, OUT_1				#carica l'indirizzo di OUT in $t0
				sb $s3, 0($t0)				#salva l'unita' nell'indirizzo OUT_1 (0x10010008)
				sb $s4, 1($t0)				#salva le decine nell'indirizzo OUT_2 (0x10010009)
				sb $s5, 2($t0)				#salva le centinaia nell'indirizzo OUT_3 (0x1001000a)

#Loop finale per continuare a controllare i dati in entrata e ristamparli

				j main					#salta a main per continuare a eseguire il programma dall'inizio